
/*Used Gulp Plugins*/
var gulp = require('gulp');
var sass = require('gulp-sass');

/*minify css*/
var cssnano = require('gulp-cssnano');

/*minify js*/
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');

/*auto reload*/
var browserSync = require('browser-sync').create();

/*clean dist resources*/
var del = require('del');

/*building files*/
var runSequence = require('run-sequence');

var merge = require('merge-stream');

var vendors = ['jquery/dist', 'bootstrap/dist', 'object-fit-polyfill/dist'];

/*Geting files from node_modules*/
gulp.task('vendors', function() {
  return merge(vendors.map(function(vendor) {
    return gulp.src('node_modules/' + vendor + '/**/*')
    .pipe(gulp.dest('app/vendors/' + vendor.replace(/\/.*/, '')));
  }));
});

gulp.task('sass', function() {
  return gulp.src('app/scss/**/*.scss') // Gets all files ending with .scss in app/scss
  .pipe(sass())
  .pipe(gulp.dest('app/css'))
  .pipe(browserSync.reload({
    stream: true
  }))
});

gulp.task('watch', ['browserSync', 'sass'],  function(){
  gulp.watch('app/scss/**/*.scss', ['sass']);

  gulp.watch('app/partials/*.html', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload);
})

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: './'
    },
    startPath: "/app/partials/index.html"
  })
})

gulp.task('getTemplates', function(){
  return gulp.src('app/partials/*.html')
  .pipe(useref())
  .pipe(gulp.dest('dist'))
});


gulp.task('getStyles', function(){
  return gulp.src('app/css/*.css')
  .pipe(useref())
  // Minifies only if it's a CSS file
  .pipe(gulpIf('*.css', cssnano()))
  .pipe(gulp.dest('dist/css'))
});

gulp.task('getJavascript', function(){
  return gulp.src('app/partials/*.html')
  .pipe(useref())
  // Minifies only if it's a JavaScript file
  .pipe(gulpIf('*.js', uglify()))
  .pipe(gulp.dest('dist'))
});



gulp.task('clean:dist', function(callback) {
  return del.sync(['dist/**/*', '!dist/images', '!dist/images/**/*'], callback)
})


gulp.task('build', function (callback) {
  runSequence(['clean:dist', 'sass', 'getTemplates', 'getStyles', 'getJavascript'],  callback  )
  console.log(' Building files -----> "DIST"');
})

gulp.task('default', function (callback) {
  runSequence(['sass', 'browserSync', 'watch'],
  callback
)
})
